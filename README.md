TopCoder Marathon Match 112 My Solution
=======================================


Problem Statement   
https://www.topcoder.com/challenges/30107461    


Standings  
https://www.topcoder.com/challenges/30107461?tab=submissions   


Forum  
https://apps.topcoder.com/forums/?module=Category&categoryID=83376  


Review Page (New Platform)  
https://submission-review.topcoder.com/challenges/30107461  


Review Page (Old Platform)  
https://software.topcoder.com/review/actions/ViewProjectDetails?pid=30107461  