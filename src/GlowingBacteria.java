import java.io.*;
import java.util.*;

public class GlowingBacteria {

    public static void main(String[] args) throws Exception {
        Main.main(args);
    }

    static int[][] indexes = null;
    static LimitTimer timer = null;

    int N, C, D, K;
    char[] finalGrid;

    public char[] findSolution(int N, int C, int D, int K, char[] grid) {
        timer = new LimitTimer(9000);
        this.N = N;
        this.C = C;
        this.D = D;
        this.K = K;
        this.finalGrid = grid;
        makeIndexes();

        System.err.println("N: " + N);
        System.err.println("C: " + C);
        System.err.println("D: " + D);
        System.err.println("K: " + K);

        return solveByNextNextNextDays(2000);
    }

    char[] solve(long limit) {
        char[] bestGrid = java.util.Arrays.copyOf(finalGrid, finalGrid.length);
        int bestScore = calcScore(bestGrid);
        java.util.Random rand = new java.util.Random(19831983);
        LimitTimer interval = timer.makeIntervalTimer(limit);
        int upd = 0;
        for (int cy = 0; cy < 1000000; cy++) {
            if (interval.isOver()) {
                System.err.println("stop: " + cy);
                break;
            }
            char[] tmpGrid = java.util.Arrays.copyOf(bestGrid, bestGrid.length);
            int y = rand.nextInt(N);
            int x = rand.nextInt(N);
            int h = rand.nextInt(N / 4) + 1;
            int w = rand.nextInt(N / 4) + 1;
            char col = (char)(rand.nextInt(C) + '0');
            for (int dy = 0; dy < h && y + dy < N; dy++) {
                for (int dx = 0; dx < w && x + dx < N; dx++) {
                    tmpGrid[(y + dy) * N + (x + dx)] = col;
                }
            }
            int tmpScore = calcScore(tmpGrid);
            if (tmpScore > bestScore) {
                upd++;
                bestGrid = tmpGrid;
                bestScore = tmpScore;
            }
        }
        System.err.println("update: " + upd);
        return bestGrid;
    }

    int calcScore(char[] grid) {
        for (int i = 0; i < D; i++) {
            grid = getNextDay(grid);
        }
        return countSames(grid);
    }

    int countSames(char[] grid) {
        int count = 0;
        for (int i = 0; i < finalGrid.length; i++) {
            if (finalGrid[i] == grid[i]) { count++; }
        }
        return count;
    }

    char[] solveByNextNextNextDays(long limit) {
        char[] firstDayGrid = java.util.Arrays.copyOf(finalGrid, finalGrid.length);
        char[][] dailyGrid = new char[D + 1][];
        dailyGrid[0] = java.util.Arrays.copyOf(firstDayGrid, firstDayGrid.length);
        for (int i = 1; i <= D; i++) {
            dailyGrid[i] = getNextDay(dailyGrid[i - 1]);
        }
        int day = D;
        int bestScore = 0;
        LimitTimer interval = timer.makeIntervalTimer(limit);
        for (int cy = 0; cy < 1000000; cy++) {
            if (interval.isOver()) {
                System.err.println("stop: " + cy);
                break;
            }
            int nextDay = day + 1;
            if (nextDay > D) { nextDay = 0; }
            int score = countSames(dailyGrid[day]);
            if (score > bestScore) {
                System.err.println("update: " + cy);
                System.arraycopy(dailyGrid[nextDay], 0, firstDayGrid, 0, firstDayGrid.length);
                bestScore = score;
            }
            getNextDay(dailyGrid[day], dailyGrid[nextDay]);
            day = nextDay;
        }
        return firstDayGrid;
    }

    void makeIndexes() {
        indexes = new int[N * N][];
        int[] ind = new int[50];
        int p = 0;
        for (int y = 0; y < N; y++) {
            for (int x = 0; x < N; x++) {
                int s = 0;
                for (int dy = -3; dy <= 3; dy++) {
                    if (y + dy >= 0 && y + dy < N) {
                        for (int dx = -3; dx <= 3; dx++) {
                            if (dx * dx + dy * dy <= K && x + dx >= 0 && x + dx < N) {
                                ind[s] = (y + dy) * N + (x + dx);
                                if (ind[s] != p) {
                                    s++;
                                }
                            }
                        }
                    }
                }
                indexes[p] = java.util.Arrays.copyOf(ind, s);
                p++;
            }
        }
    }

    int[] getFreq(char[] grid, int x, int y) {
        int[] e = new int[C];
        int[] ind = indexes[y * N + x];
        for (int i = 0; i < ind.length; i++) {
            e[grid[ind[i]] - '0']++;
        }
        return e;
    }

    char[] getNextDay(char[] src) {
        return getNextDay(src, new char[N * N]);
    }

    char[] getNextDay(char[] src, char[] dst) {
        int p = 0;
        for (int y = 0; y < N; y++) {
            for (int x = 0; x < N; x++) {
                int[] e = getFreq(src, x, y);
                for (int i = 0; i < e.length; i++) {
                    if (e[i] == 0) {
                        e[i] = 255 * C + i;
                    } else {
                        e[i] = e[i] * C + i;
                    }
                }
                java.util.Arrays.sort(e);
                if (e[0] / C < e[1] / C) {
                    dst[p] = (char)((e[0] % C) + '0');
                } else {
                    dst[p] = src[p];
                }
                p++;
            }
        }
        return dst;
    }

    char[] mergeFreq(char[] src) {
        return mergeFreq(src, new char[N * N]);
    }

    char[] mergeFreq(char[] src, char[] dst) {
        int p = 0;
        for (int y = 0; y < N; y++) {
            for (int x = 0; x < N; x++) {
                int[] e = getFreq(src, x, y);
                for (int i = 0; i < e.length; i++) {
                    e[i] = e[i] * C + i;
                }
                java.util.Arrays.sort(e);
                if (e[C - 1] / C > e[C - 2] / C) {
                    dst[p] = (char)((e[C - 1] % C) + '0');
                } else {
                    dst[p] = src[p];
                }
                p++;
            }
        }
        return dst;
    }


}

class LimitTimer {
    long limitTime;
    LimitTimer(long limit) {
        limitTime = System.currentTimeMillis() + limit;
    }
    boolean isOver() {
        return System.currentTimeMillis() >= limitTime;
    }
    LimitTimer makeIntervalTimer(long limit) {
        return new IntervalTimer(limit, limitTime);
    }
    static class IntervalTimer extends LimitTimer {
        long lastTime;
        long interval = 0;
        IntervalTimer(long limit, long totalLimit) {
            super(limit);
            lastTime = limitTime - limit;
            limitTime = Math.min(limitTime, totalLimit);
        }
        @Override
        boolean isOver() {
            long now = System.currentTimeMillis();
            interval = Math.max(interval, now - lastTime);
            lastTime = now;
            return now + interval >= limitTime;
        }
    }
}

class Main {

    static int callFindSolution(BufferedReader in, GlowingBacteria glowingBacteria) throws Exception {

        int N = Integer.parseInt(in.readLine());
        int C = Integer.parseInt(in.readLine());
        int D = Integer.parseInt(in.readLine());
        int K = Integer.parseInt(in.readLine());
        int _gridSize = N * N;
        char[] grid = new char[_gridSize];
        for (int _idx = 0; _idx < _gridSize; _idx++) {
            grid[_idx] = in.readLine().charAt(0);
        }

        long start_time = System.currentTimeMillis();

        char[] _result = glowingBacteria.findSolution(N, C, D, K, grid);

        long end_time = System.currentTimeMillis();
        System.err.printf("time: %d ms%n", end_time - start_time);

        System.out.println(_result.length);
        for (char _it : _result) {
            System.out.println(_it);
        }
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        GlowingBacteria glowingBacteria = new GlowingBacteria();

        // do edit codes if you need

        callFindSolution(in, glowingBacteria);

    }

}
